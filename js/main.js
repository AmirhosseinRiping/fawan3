$( function() {
    // run the currently selected effect
    function runEffect() {
      // get effect type from
      var selectedEffect = "fold";
 
      // Most effect types need no options passed by default
      var options = {};
      // some effects have required parameters
      if ( selectedEffect === "scale" ) {
        options = { percent: 50 };
      } else if ( selectedEffect === "size" ) {
        options = { to: { width: 100, height: 185 } };
      }
 
      // Run the effect
      $( "#pending" ).show( selectedEffect, options, 500, callback );
    };
 
    //callback function to bring a hidden box back
    function callback() {
      setTimeout(function() {
        $( "#effect:visible" ).removeAttr( "style" ).fadeOut();
      }, 1000 );
    };
 
    // Set effect from select menu value
    $( "#show" ).on( "click", function() {
      runEffect();
    });
 
    $( "#pending" ).hide();
  } );

$( function() {
    var state = true;
    $( "#show" ).on( "click", function() {
      if ( state ) {
        $( "#effect" ).animate({
          width: 150
        }, 1000 );
      } else {
        $( "#effect" ).animate({
          width: 300
        }, 1000 );
      }
      state = !state;
    });
  } );


  $(document).ready(function(){
    $("#show").click(function(){
      $("#hiden").empty();
    });
  });
  

  $( function() {
    var state = true;
    $( "#show" ).on( "click", function() {
      if ( state ) {
        $( "#saiz" ).animate({
          width: 900
        }, 1000 );
      } else {
        $( "#saiz" ).animate({
          width: 800
        }, 1000 );
      }
      state = !state;
    });
  } );
  